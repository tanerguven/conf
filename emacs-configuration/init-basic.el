(setq-default tab-width 4)
(menu-bar-mode 0)

;; hide-mode-line
(setq-default mode-line-format nil)

(delete-selection-mode 1)

(global-set-key "\C-z" nil)
(global-set-key "\C-\M-z" 'suspend-emacs)
(global-set-key "\C-z\C-z" 'comment-or-uncomment-region)

(global-set-key "\M-n" 'forward-paragraph)
(global-set-key "\M-p" 'backward-paragraph)
