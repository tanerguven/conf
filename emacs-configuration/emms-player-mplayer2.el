;;; emms-player-mplayer.el --- mplayer support for EMMS

;; Copyright (C) 2005, 2006, 2007, 2008, 2009 Free Software Foundation, Inc.

;; Authors: William Xu <william.xwl@gmail.com>
;;          Jorgen Schaefer <forcer@forcix.cx>

;; This file is part of EMMS.

;; EMMS is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; EMMS is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with EMMS; if not, write to the Free Software Foundation,
;; Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

;;; Commentary:

;; This provides a player that uses mplayer. It supports pause and
;; seeking. For loading subtitles automatically, try adding
;; "sub-fuzziness=1" to your `~/.mplayer/config', see mplayer manual for
;; more.

;;; Code:

(require 'emms-compat)
(require 'emms-player-simple)

(define-emms-simple-player mplayer2 '(file url)
  (concat "\\`\\(http[s]?\\|mms\\)://\\|"
	  (apply #'emms-player-simple-regexp
		 emms-player-base-format-list))
  "mplayer" "-quiet" "-really-quiet" "-vo" "null")

(define-emms-simple-player mplayer2-playlist '(streamlist)
  "\\`http[s]?://"
  "mplayer" "-quiet" "-really-quiet" "-playlist" "-vo" "null")

(emms-player-set emms-player-mplayer2
		 'pause
		 'emms-player-mplayer2-pause)

;;; Pause is also resume for mplayer2
(emms-player-set emms-player-mplayer2
                 'resume
                 nil)

(emms-player-set emms-player-mplayer2
		 'seek
		 'emms-player-mplayer2-seek)

(emms-player-set emms-player-mplayer2
		 'seek-to
		 'emms-player-mplayer2-seek-to)

(defun emms-player-mplayer2-pause ()
  "Depends on mplayer2's -slave mode."
  (process-send-string
   emms-player-simple-process-name "pause\n"))

(defun emms-player-mplayer2-seek (sec)
  "Depends on mplayer2's -slave mode."
  (process-send-string
   emms-player-simple-process-name
   (format "seek %d\n" sec)))

(defun emms-player-mplayer2-seek-to (sec)
  "Depends on mplayer2's -slave mode."
  (process-send-string
   emms-player-simple-process-name
   (format "seek %d 2\n" sec)))

(provide 'emms-player-mplayer2)
;;; emms-player-mplayer2.el ends here
