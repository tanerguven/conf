;; http://www.emacswiki.org/emacs/MoveLine
(defun move-line-up ()
  (interactive)
  (transpose-lines 1)
  (forward-line -2))
(defun move-line-down ()
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
    (forward-line -1))

(defun xclip-mode()
  (interactive)
  (load-file "~/.emacs.d/emacs-configuration/xclip.el")
  (turn-on-xclip)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                   Default Modes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun tnr-default-mode()
  (menu-bar-mode 0)

  ;; dosya acma vb. islemlerde fuzzy matching kullanir
  (require 'ido)
  (ido-mode t)
  (setq ido-enable-flex-matching t)

  ;; delete seleted text when typing
  (delete-selection-mode 1)
  ;; default tab width
  (setq-default tab-width 4)
  ;; altta cursorun bulundugu satir ve sutun numaralarini yazar
  (column-number-mode 1)
  ;; white space
  (setq-default show-trailing-whitespace t)
  ;; auto complete
  (require 'auto-complete)

;; (add-to-list 'auto-mode-alist '("\\.py$" . programming-mode-python))
  )

(defun programming-mode()
  ;; Line numbering
  (setq linum-format "%4d")
  (global-linum-mode 1)
  ;; highlight brackets
  (show-paren-mode t)
  )

(defun programming-mode-python()
  (programming-mode)
  (load-file "~/.emacs.d/emacs-configuration/modules/emacs-for-python/epy-init.el")
  (setq skeleton-pair nil) ;; disable "(' pairing
  )

(defun programming-mode-verilog()
  (programming-mode)
  (setq
   verilog-indent-level 2
   verilog-indent-level-module 2
   verilog-indent-level-declaration 2
   verilog-indent-level-behavioral 2
   verilog-indent-level-directive 1
   verilog-case-indent 2
   verilog-auto-newline nil
   verilog-auto-indent-on-newline nil
   verilog-tab-always-indent t
   verilog-auto-endcomments nil
   verilog-highlight-p1800-keywords nil
   indent-tabs-mode nil
   )
  )

(defun programming-mode-c()
  ;; FIXME: https://github.com/Sarcasm/flycheck-irony

  (programming-mode)
  (setq c-default-style "linux" c-basic-offset 4)

  ;; ---- irony-mode ----
  ;; required packages: elpa-company libclang-dev clang
  (add-to-list 'load-path "~/.emacs.d/emacs-configuration/modules/irony-mode/")
  (load-file "~/.emacs.d/emacs-configuration/modules/irony-mode/irony.el")
  (irony-mode)
  ;;(irony-cdb-autosetup-compile-options)

  ;; --- company-mode && company-irony ---
  (load-file "~/.emacs.d/emacs-configuration/modules/company-irony.el")
  (eval-after-load 'company
  	'(add-to-list 'company-backends 'company-irony))
  (company-mode)

  ;; --- company-c-headers ---
  ;; https://github.com/randomphrase/company-c-headers/
  (load-file "~/.emacs.d/emacs-configuration/modules/company-c-headers.el")
  (add-to-list 'company-backends 'company-c-headers)
  (setq company-c-headers-path-system (list "/usr/include" "/usr/include/c++/6/" "/usr/local/include"))

  ;; --- irony-eldoc ---
  ;; https://github.com/ikirill/irony-eldoc/
  (load-file "~/.emacs.d/emacs-configuration/modules/irony-eldoc.el")
  (irony-eldoc)

  ;; company dark background fix
  ;; https://github.com/company-mode/company-mode/issues/380
  (custom-set-faces
   '(company-preview
	 ((t (:foreground "darkgray" :underline t))))
   '(company-preview-common
	 ((t (:inherit company-preview))))
   '(company-tooltip
	 ((t (:background "lightgray" :foreground "black"))))
   '(company-tooltip-selection
	 ((t (:background "steelblue" :foreground "white"))))
   '(company-tooltip-common
	 ((((type x)) (:inherit company-tooltip :weight bold))
	  (t (:inherit company-tooltip))))
   '(company-tooltip-common-selection
	 ((((type x)) (:inherit company-tooltip-selection :weight bold))
	  (t (:inherit company-tooltip-selection)))))

  ;; semantic mode ayalari
  (custom-set-variables
   ;;'(semantic-idle-scheduler-idle-time 0.1) ;; kod tamamlama gecikmesi
   '(global-semantic-idle-summary-mode nil) ;; asagidaki fonksiyon/degisken bilgisi
   '(global-semantic-highlight-func-mode nil) ;; fonksiyonun icindeyken fonksiyonu renklendirme
   '(global-semantic-idle-local-symbol-highlight-mode nil) ;; cursorun uzerinde oldugu degiskenin gectigi yerleri renklendirme
   '(global-semantic-decoration-mode nil) ;; class icinde protected/private tanimlarin arka planini renklendirmeyi iptal et
   )
  (semantic-mode 1)
  (semanticdb-enable-gnu-global-databases 'c-mode)
  (semanticdb-enable-gnu-global-databases 'c++-mode)
  ;; ;; (semantic-add-system-include "/usr/local/include/" 'c-mode)
  ;; ;; (semantic-add-system-include "/usr/local/include/" 'c++-mode)
  ;; (global-semantic-idle-completions-mode 1)
  ;; ;; (hs-minor-mode t)
  )

(defun programming-mode-java()
  (programming-mode)
  (load-file "~/.emacs.d/emacs-configuration/_auto-java-complete/yasnippet/yasnippet.el")
  (load-file "~/.emacs.d/emacs-configuration/_auto-java-complete/ajc-java-complete.el")
  (load-file "~/.emacs.d/emacs-configuration/_auto-java-complete/ajc-java-complete-config.el")
  (setq ajc-tag-file-list (list (expand-file-name "~/.java_base.tag")))
  (ajc-java-complete-mode 1)
  (add-hook 'find-file-hook 'ajc-4-jsp-find-file-hook)
  ;; FIXME: yasnippet kapanmama probleminin gecici cozumu
  (local-set-key (kbd "RET") (lambda() (interactive) (insert "\n") (yas-exit-all-snippets)))
  )

(defun programming-mode-javascript-2()
  ;; (programming-mode)
  ;; (js2-mode-toggle-warnings-and-errors)
  ;; (auto-complete-mode)
  (js-mode)
 )

(defun programming-mode-javascript()
  (programming-mode)
  (auto-complete-mode)
 )

(defun open-music-player (&optional arg)
  "open-music-player"
  (interactive "p")

  (require 'emms-setup)
  (require 'emms-source-file)
  (require 'emms-source-playlist)
  (emms-standard)
  (emms-default-players)
  (setq emms-cache nil)
  (load-file "~/.emacs.d/emacs-configuration/emms-player-mplayer2.el")
  (setq emms-player-list '(emms-player-mplayer2))
  (global-set-key [(meta f8)] 'emms-playlist-mode-go)

  (call-interactively 'emms-play-directory-tree)
  (call-interactively 'emms-playlist-mode-go)
  )


(add-hook 'python-mode-hook 'programming-mode-python)
(add-hook 'c-mode-hook 'programming-mode-c)
(add-hook 'c++-mode-hook 'programming-mode-c)
(add-hook 'verilog-mode-hook 'programming-mode-verilog)
(add-hook 'asm-mode-hook 'programming-mode)
(add-hook 'java-mode-hook 'programming-mode-java)
(add-hook 'js2-mode-hook 'programming-mode-javascript-2)
(add-hook 'js-mode-hook 'programming-mode-javascript)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                         KEYS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global-set-key "\C-z" nil)
(global-set-key "\C-\M-z" 'suspend-emacs)

(global-set-key "\C-z\C-z" 'comment-or-uncomment-region)

(global-set-key "\M-n" 'forward-paragraph)
(global-set-key "\M-p" 'backward-paragraph)
(global-set-key "\M-P" 'move-line-up)
(global-set-key "\M-N" 'move-line-down)

(global-set-key "\C-x\C-b" 'ibuffer)

;; playlist
(global-set-key "\C-c\C-o" 'open-music-player)

;; gcl-contacts
;; (require 'gcl-contacts)
;; (global-set-key "\C-c\C-l" 'gcl-contact-complete)

(defun hide-mode-line (&optional arg)
  "hide-mode-line"
  (interactive "p")
  (setq mode-line-format nil)
)
(global-set-key (kbd "C-c h") 'hide-mode-line)

;; c-mode
(add-hook
 'c-mode-common-hook
 (lambda()
   (local-set-key (kbd "C-z C-s") 'hs-show-block)
   (local-set-key (kbd "C-z C-d") 'hs-hide-block)
   (local-set-key (kbd "C-z C-l") 'hs-hide-level)
   (local-set-key (kbd "C-z C-e") 'hs-show-block)
   (local-set-key (kbd "C-z C-r") 'hs-hide-all)
   (local-set-key (kbd "C-z C-a") 'hs-show-all)

   (local-set-key "\C-cj" 'semantic-ia-fast-jump)
   (local-set-key "\C-cq" 'semantic-ia-show-doc)
   (local-set-key "\C-cs" 'semantic-ia-show-summary)
   (local-set-key "\C-cp" 'semantic-analyze-proto-impl-toggle)

   (local-set-key (kbd "TAB") 'irony--indent-or-complete)
   (local-set-key [tab] 'irony--indent-or-complete)

   ;; (local-set-key "\C-ce" 'eassist-list-methods) ;; fonksiyonlarin listesini gosterir
   ;; (local-set-key "\C-c\C-r" 'semantic-symref)

   (local-set-key (kbd "TAB") 'irony--indent-or-complete)
   (local-set-key [tab] 'irony--indent-or-complete)
   ))

(tnr-default-mode)

;; bind TAB for indent-or-complete
;; https://tuhdo.github.io/c-ide.html
(defun irony--check-expansion ()
  (save-excursion (if (looking-at "\\_>") t (backward-char 1) (if (looking-at "\\.") t (backward-char 1) (if (looking-at "->") t nil)))))
(defun irony--indent-or-complete () "Indent or Complete" (interactive)(cond ((and (not (use-region-p)) (irony--check-expansion)) (message "complete") (company-complete-common)) (t (message "indent") (call-interactively 'c-indent-line-or-region))))
