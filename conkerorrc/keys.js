undefine_key(default_global_keymap, "q"); // kill-current-buffer

undefine_key(content_buffer_normal_keymap, "z i"); // zoom
undefine_key(content_buffer_normal_keymap, "z m"); // zoom
undefine_key(content_buffer_normal_keymap, "z z"); // zoom
undefine_key(content_buffer_normal_keymap, "z o"); // zoom
undefine_key(content_buffer_normal_keymap, "z r"); // zoom
undefine_key(content_buffer_normal_keymap, "z I"); // zoom
undefine_key(content_buffer_normal_keymap, "z o"); // zoom
undefine_key(content_buffer_normal_keymap, "z M"); // zoom
undefine_key(content_buffer_normal_keymap, "z Z"); // zoom
undefine_key(content_buffer_normal_keymap, "z O"); // zoom
undefine_key(content_buffer_normal_keymap, "z R"); // zoom
undefine_key(content_buffer_normal_keymap, "z +"); // zoom
undefine_key(content_buffer_normal_keymap, "z ="); // zoom
undefine_key(content_buffer_normal_keymap, "z -"); // zoom
undefine_key(content_buffer_normal_keymap, "+"); // zoom
undefine_key(content_buffer_normal_keymap, "="); // zoom
undefine_key(content_buffer_normal_keymap, "-"); // zoom

undefine_key(content_buffer_normal_keymap, "B"); // back
undefine_key(content_buffer_normal_keymap, "F"); // forward
undefine_key(content_buffer_normal_keymap, "u"); // up
undefine_key(content_buffer_normal_keymap, "r");
undefine_key(content_buffer_normal_keymap, "g");
undefine_key(content_buffer_normal_keymap, "G");
undefine_key(content_buffer_normal_keymap, "l");
undefine_key(content_buffer_normal_keymap, "i");
undefine_key(content_buffer_normal_keymap, "n");
undefine_key(content_buffer_normal_keymap, "m");
undefine_key(content_buffer_normal_keymap, "e");
undefine_key(content_buffer_normal_keymap, "T");
undefine_key(content_buffer_normal_keymap, "#");
undefine_key(content_buffer_normal_keymap, "c"); // copy link
undefine_key(content_buffer_normal_keymap, "s"); // save
undefine_key(content_buffer_normal_keymap, "\\"); // view source
undefine_key(content_buffer_normal_keymap, "<"); // scroll
undefine_key(content_buffer_normal_keymap, ">"); // scroll
undefine_key(content_buffer_normal_keymap, "v");
undefine_key(content_buffer_normal_keymap, "b"); // bookmark
undefine_key(content_buffer_normal_keymap, "x"); // shell command on file
undefine_key(content_buffer_normal_keymap, "X"); // shell command on url
undefine_key(content_buffer_normal_keymap, "d"); // delete DOM node
undefine_key(content_buffer_normal_keymap, ";"); // focus
undefine_key(content_buffer_normal_keymap, "f"); // follow
undefine_key(content_buffer_normal_keymap, "t"); // follow top

undefine_key(special_buffer_keymap, "S-space"); // pageup
undefine_key(special_buffer_keymap, "backspace"); // pageup
undefine_key(special_buffer_keymap, "space"); // pagedown
undefine_key(special_buffer_keymap, "page_up"); // pageup
undefine_key(special_buffer_keymap, "page_down"); // pagedown
undefine_key(special_buffer_keymap, "up");
undefine_key(special_buffer_keymap, "down");
undefine_key(special_buffer_keymap, "left");
undefine_key(special_buffer_keymap, "right");
undefine_key(special_buffer_keymap, "home");
undefine_key(special_buffer_keymap, "end");

undefine_key(content_buffer_normal_keymap, "S-space"); // pageup
undefine_key(content_buffer_normal_keymap, "backspace"); // pageup
undefine_key(content_buffer_normal_keymap, "space"); // pagedown
undefine_key(content_buffer_normal_keymap, "page_up"); // pageup
undefine_key(content_buffer_normal_keymap, "page_down"); // pagedown
undefine_key(content_buffer_normal_keymap, "up");
undefine_key(content_buffer_normal_keymap, "down");
undefine_key(content_buffer_normal_keymap, "left");
undefine_key(content_buffer_normal_keymap, "right");
undefine_key(content_buffer_normal_keymap, "home");
undefine_key(content_buffer_normal_keymap, "end");


undefine_key(content_buffer_normal_keymap, "S"); // i-search forward
undefine_key(content_buffer_normal_keymap, "R"); // i-search backward


undefine_key(default_global_keymap, "M-S-return"); // full screen

undefine_key(default_global_keymap, "C-x C-c"); // quit conkeror



define_key(default_global_keymap, "C-c f", "follow-new-buffer-background");
define_key(default_global_keymap, "C-c C-f", "follow");

// C-x C-v		find-alternate-url
// C-x C-s		save-page
// C-x return c	charset-prefix
// C-x return r	reload-with-charset
// C-x C-c		quit
// C-x b		switch-to-buffer
// C-x k		kill-buffer

// simdiki url'yi duzenlemek
// C-x C-v		find-alternate-url
// C-x C-f		find-url-new-buffer
define_key(default_global_keymap, "C-x f", "find-url-from-history-new-buffer");

define_key(default_global_keymap, "C-c C-s", "shell-command-on-file");
define_key(default_global_keymap, "C-c C-n", "forward");
define_key(default_global_keymap, "C-c C-p", "back");
define_key(default_global_keymap, "C-c C-r", "reload");


// shell-command-on-url ???


define_key(content_buffer_normal_keymap, "C-d", "darken-page");
