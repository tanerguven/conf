homepage = "about:blank";

minibuffer_auto_complete_default = true;
url_completion_use_history = true;
url_completion_use_bookmarks = true;

//  integration with emacs
editor_shell_command = "emacsclient -c";



if ('@eff.org/https-everywhere;1' in Cc) {
	interactive("https-everywhere-options-dialog",
				"Open the HTTPS Everywhere options dialog.",
				function (I) {
					window_watcher.openWindow(
						null, "chrome://https-everywhere/content/preferences.xul",
						"", "chrome,titlebar,toolbar,centerscreen,resizable", null);
				});
}

// session_pref("xpinstall.whitelist.required", false);

require("noscript");
