#!/bin/bash

pkill nm-applet
pkill stalonetray
pkill polkit-gnome-authentication-agent-1
pkill gnome-screen-saver

sleep 0.5

gnome-settings-daemon &
sudo /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 &
stalonetray &
nm-applet &
gnome-screensaver &
